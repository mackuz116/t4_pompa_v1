/*
 * pompa.c
 *
 *  Created on: Nov 4, 2020
 *      Author: maciej
 */


#include "main.h"
#include "pompa.h"
#include "usart.h"

void PompaInit(TPompa* Key, uint8_t pozycja,  uint8_t Aktywna, uint8_t Dawka, uint8_t StartGodzina, uint8_t StartMinuta, uint8_t *DniTygodnia, uint8_t Stan, uint8_t Licznik)
{
    Key->Aktywna[pozycja] = Aktywna;
    Key->Dawka[pozycja] = Dawka;
    Key->StartGodzina[pozycja] = StartGodzina;
    Key->StartMinuta[pozycja] = StartMinuta;
    for( uint8_t i= 0; i<7; i++) Key->DniTygodnia[pozycja][i] = DniTygodnia[i];
    Key->Stan[pozycja] = Stan;
    Key->Licznik[pozycja] = Licznik;
}

