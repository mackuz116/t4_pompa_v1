/*
 * pompa.h
 *
 *  Created on: Nov 4, 2020
 *      Author: maciej
 */

#ifndef INC_POMPA_H_
#define INC_POMPA_H_

#define S_DPD 5 // ilość dopuszczalnych podań dziennie (ile razy dziennie można uruchomić pompę -
				// założenie uruchomienia nie mogą następić w tej samej minucie)
typedef struct pompa_struct
{
	uint8_t		Aktywna[S_DPD];
	uint8_t		Dawka[S_DPD];
	uint8_t		StartGodzina[S_DPD];
	uint8_t		StartMinuta[S_DPD];
	uint8_t		DniTygodnia[S_DPD][7];
	uint8_t		Stan[S_DPD];
	uint8_t		Licznik[S_DPD];
} TPompa;

void PompaInit(TPompa* Key, uint8_t pozycja,  uint8_t Aktywna, uint8_t Dawka, uint8_t StartGodzina, uint8_t StartMinuta, uint8_t *DniTygodnia, uint8_t Stan, uint8_t Licznik);

#endif /* INC_POMPA_H_ */
